FROM php:7.4-apache

RUN wget https://raw.githubusercontent.com/composer/getcomposer.org/1b137f8bf6db3e79a38a5bc45324414a6b1f9df2/web/installer -O - -q | php -- --quiet
RUN mv composer.phar /usr/local/bin/composer

COPY . /usr/src/myapp
WORKDIR /usr/src/myapp

RUN composer install

EXPOSE 8000

ENTRYPOINT ["php artisan serve"]
